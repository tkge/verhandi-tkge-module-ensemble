# Ensemble Module

Ensemble Module of `Verðandi`

## Installation

Install requirements using [poetry](https://python-poetry.org/).

```bash
poetry install
```

or [pip](https://pip.pypa.io/en/stable/)

```bash
pip install -r requirements.txt
```

## Usage

Enter hostnames and ports of desired TKGE modules into `config.ini`, under sections `[server{n}]` where `{n}` is the number for that server.
Any number of servers is allowed, and all will be contacted when computing link prediction.

Also enter desired hostname, port and number of workers, under section `[server]`

Run using `main.py`. Should be used in tandem with [Verðandi UI and NL modules](https://gitlab.com/tkge/verhandi-ui-nl) and [Verðandi TKGE modules](https://gitlab.com/tkge/verdandi-tkge-modules-de).

## Development

To include a new method of ensembling, create a new implementation of `Ensemble`, where method `ensemble_predict_link` takes generated class `LinkPredictionQuery` and returns generated class `LinkPredictionResponse`.

See `ensemble_ranker.py` and `weighted_ensemble_ranker.py`, for inspiration.


## License

[MIT](https://choosealicense.com/licenses/mit/)
