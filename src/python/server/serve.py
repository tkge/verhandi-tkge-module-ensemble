from concurrent import futures

import grpc
import logging


import src.python.generated.link_prediction_pb2_grpc as lp_pb2_grpc

from ..config import Config
from ..servicer import EnsembleServicer

_LOGGER = logging.getLogger()


def serve(ensemble_servicer: EnsembleServicer, config: Config) -> None:
    """Run server"""

    # Configure server threadpool and add servicer to server.
    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=config.getint("server", "max_workers"))
    )
    lp_pb2_grpc.add_LinkPredictionServicer_to_server(ensemble_servicer, server)

    # Add insecure port to server. TODO: Change to secure port (for production)
    server.add_insecure_port(
        f"{config.get('server', 'host')}:{config.get('server', 'port')}"
    )

    # Start server, and block.
    _LOGGER.info("Starting server...")
    server.start()
    _LOGGER.info("Server ready!")
    server.wait_for_termination()
