import grpc
import logging

from ..config import Config
from ..ensemble import Ensemble
from ..generated import link_prediction_pb2 as pb2
from ..generated import link_prediction_pb2_grpc as pb2_grpc

_LOGGER = logging.getLogger()


class EnsembleServicer(pb2_grpc.LinkPredictionServicer):
    """
    Implementation of generated LinkPredictionServicer.
    Instantiate this, and register it using gRPC.
    When gRPC API `PredictLink` is called, this class' `PredictLink` method is used.
    This is, by design, using exact same API as TKGE module,
    so this can be used between NL2Q module and TKGE module.
    """

    def __init__(self, ensemble: Ensemble, config: Config) -> None:
        """
        Create instance of EnsembleServicer, using dependency injection for
        configuration, and specific ensemble implementation.

        Args:
            ensemble (Ensemble): Implementation of ensemble to use.
            config (Config): Configuration file as object.
        """
        super().__init__()
        self.ensemble = ensemble
        self.config = config

    def PredictLink(
        self, request: pb2.LinkPredictionQuery, context: grpc.ServicerContext
    ) -> pb2.LinkPredictionResponse:
        """
        Do link prediction on all underlying TKGE modules,
        and use the chosen ensemble implementation to combine results.
        """
        return self.ensemble.ensemble_predict_link(request)
