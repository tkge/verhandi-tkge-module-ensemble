from abc import ABC, abstractmethod

from ..generated import link_prediction_pb2 as pb2


class Ensemble(ABC):
    @abstractmethod
    def ensemble_predict_link(
        self,
        link_prediction_query: pb2.LinkPredictionQuery,
    ) -> pb2.LinkPredictionResponse:
        """Do link prediction on underlying TKGE modules, and use ensemble to combine results.

        Args:
            link_prediction_query (pb2.LinkPredictionQuery):
            Query to do link prediction with.
            Contains TKG fact and desired number of results.
            Number of results, also determines how many results each TKGE will return,
            so n=10 means each TKGE will return 10 results, then these n*TKGE's will be
            combined and reduced to n, reranked, and returned.

        Returns:
            pb2.LinkPredictionResponse: Response containing n TKG facts.
        """
        raise NotImplementedError
