import json
from typing import Dict, Tuple

import grpc
import logging

from ...config import Config
from ...generated import link_prediction_pb2 as pb2
from ...generated import link_prediction_pb2_grpc as pb2_grpc
from .. import Ensemble

_LOGGER = logging.getLogger()


class EnsembleRanker(Ensemble):
    def __init__(self, config: Config) -> None:
        super().__init__()
        self.servers = [
            {
                "host": config.get(section, "host"),
                "port": config.get(section, "port"),
            }
            for section in config.sections()
            if section != "server"
        ]
        _LOGGER.debug(f"EnsembleRanker created with servers: {self.servers}")

    def ensemble_predict_link(
        self,
        link_prediction_query: pb2.LinkPredictionQuery,
    ) -> pb2.LinkPredictionResponse:
        _LOGGER.info("Request received...")
        _LOGGER.debug(f"Request: {request}")
        _LOGGER.debug("Handling request...")
        channels = [
            grpc.insecure_channel(f"{server['host']}:{server['port']}")
            for server in self.servers
        ]
        _LOGGER.debug(f"Channels created: {channels}")
        stubs = [pb2_grpc.LinkPredictionStub(channel=channel) for channel in channels]
        _LOGGER.debug(f"Stubs created: {stubs}")
        _LOGGER.debug("Getting answers from TKGE servers...")
        answers = [stub.PredictLink(link_prediction_query) for stub in stubs]
        _LOGGER.debug(f"Received responses: {answers}")

        _LOGGER.debug("Calculating scores based on ranks...")

        # Create a dictionary of fact-score pairs, and fill this.
        rank_dict: Dict[Tuple[int, int, int, str], int] = {}
        for answer in answers:
            for i, fact in enumerate(answer.facts):
                factt = (fact.head, fact.relation, fact.tail, fact.time)
                old_score = rank_dict.get(factt, 0)
                # New score is calculated here.
                rank_dict[factt] = old_score + (len(answer.facts) - i)

        # Convert fact-score dictionary into list of (fact, score).
        rank_list = [
            (pb2.Fact(head=k[0], relation=k[1], tail=k[2], time=k[3], score=0), v)
            for k, v in rank_dict.items()
        ]

        # Sort and return results.
        sorted_results = sorted(rank_list, key=lambda x: x[1], reverse=True)
        n_best = sorted_results[: link_prediction_query.num_replies]
        _LOGGER.debug(f"Best answers: {n_best}")
        response = pb2.LinkPredictionResponse(facts=[x[0] for x in n_best])

        _LOGGER.debug("Returning response...")
        return response
