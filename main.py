import logging
import logging.handlers
import logging.config
from argparse import ArgumentParser
from src.python import serve
from src.python.config import Config
from src.python.ensemble import EnsembleRanker, WeightedEnsembleRanker
from src.python.servicer import EnsembleServicer


def _get_argparser() -> ArgumentParser:
    """Create argumentparse to allow for overriding configuration on CLI."""
    parser = ArgumentParser("Run an ensemble server.")
    parser.add_argument(
        "--host", type=str, dest="server.host", help="This server hostname."
    )
    parser.add_argument(
        "--port", type=str, dest="server.port", help="This server port."
    )
    parser.add_argument(
        "--max-workers",
        type=str,
        dest="server.max_workers",
        help="This server max workers.",
    )
    return parser


if __name__ == "__main__":
    # Parse CLI arguments
    parser = _get_argparser()
    args = parser.parse_args()

    # Load configuration
    logging.config.fileConfig("logging.conf")
    config = Config(**vars(args))

    # Create object for dependency injection
    ranker = WeightedEnsembleRanker(config)
    # ranker = EnsembleRanker(config)
    servicer = EnsembleServicer(ranker, config)

    # Start server, inject servicer and config
    serve(servicer, config)
